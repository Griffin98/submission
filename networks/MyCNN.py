import torch
import torch.nn as nn
import torch.nn.functional as F


"""
My custom CNN network architecture
"""
class MyCNN(nn.Module):
    # constructor
    def __init__(self, num_class):
        super(MyCNN, self).__init__()
        # first hidden layer
        self.cnn1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=5, padding=2,
                              stride=1)  # output width: 224
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)  # output width: 112

        # second hidden layer
        self.cnn2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5, padding=2,
                              stride=1)  # output width: 112
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)  # output width: 56

        # third hidden layer
        self.cnn3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=5, padding=2,
                              stride=1)  # output width: 56
        self.maxpool3 = nn.MaxPool2d(kernel_size=2)  # output width: 28

        self.fc1 = nn.Linear(128 * 28 * 28, 128)
        self.fc2 = nn.Linear(128, num_class)

        self.dropout = nn.Dropout(0.3)

    def forward(self, x):
        x = self.cnn1(x)
        x = self.dropout(x)
        x = torch.relu(x)
        x = self.maxpool1(x)
        x = self.cnn2(x)
        x = self.dropout(x)
        x = torch.relu(x)
        x = self.maxpool2(x)
        x = self.cnn3(x)
        x = self.dropout(x)
        x = torch.relu(x)
        x = self.maxpool3(x)
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        x = F.softmax(self.fc2(x), dim=1)
        return x

if __name__ == "__main__":
    img = torch.randn(4,3,224,224)

    net = MyCNN(3)
    y = net(img)
    print(y.shape)