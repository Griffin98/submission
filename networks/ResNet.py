import torch
import torch.nn as nn

from torchvision import models

"""
ResNet architecture based on ResNet18 with modified final layer.

Transfer Learning.
"""


class ResNet(nn.Module):
    # constructor
    def __init__(self, num_class):
        super(ResNet, self).__init__()
        # Load pretrained network
        self.pre_trained_resnet = models.resnet18(pretrained=True)

        # Create and replace final linear layer
        final_linear = nn.Linear(self.pre_trained_resnet.fc.in_features, num_class)
        self.pre_trained_resnet.fc = final_linear

    def forward(self, x):
        x = self.pre_trained_resnet(x)

        return x


if __name__ == "__main__":
    net = ResNet(3)
    x = torch.randn(4, 3, 224, 224)
    y = net(x)
    print(y.shape)
