import torch
import torch.nn as nn
from torchvision import models

"""
Vision Transformer architecture based on ViT_b_16 with modified head.

Transfer Learning.
"""


class ViT(nn.Module):
    def __init__(self, num_class):
        super().__init__()
        # Load pretrained network
        self.pretrained_vit = models.vit_b_16(pretrained=True)

        # Create and replace final linear layer
        heads = nn.Sequential(
            nn.Linear(self.pretrained_vit.heads.head.in_features, num_class)
        )
        self.pretrained_vit.heads = heads

    def forward(self, x):
        x = self.pretrained_vit(x)

        return x


if __name__ == "__main__":
    vit = ViT(3)
    img = torch.randn(4, 3, 224, 224)
    y = vit(img)
    print(y.shape)
