import os
import torch
import torch.nn as nn
import torch.optim as optim
import seaborn as sns
sns.set_style('darkgrid')
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

from networks.ViT import ViT


class ViTModel():
    def __init__(self, train_dataloader, val_dataloader, test_dataloader, num_classes, epochs, learning_rate,
                 device='cuda'):
        self.device = device

        self.num_epochs = epochs
        self.train_dataloader = train_dataloader
        self.test_dataloader = test_dataloader
        self.val_dataloader = val_dataloader

        self.net = ViT(num_classes)

        if torch.cuda.is_available() and self.device == "cuda":
            self.net.to("cuda")

        self.criterion = nn.CrossEntropyLoss()
        self.optimizer = optim.Adam(self.net.parameters(), lr=learning_rate)

    # Method to perform fitting of the model
    def fit(self):
        train_loss_list = []
        train_acc_list = []
        val_loss_list = []
        val_acc_list = []

        num_train = len(self.train_dataloader.dataset)
        num_val = len(self.val_dataloader.dataset)

        trainSteps = len(self.train_dataloader)
        valSteps = len(self.val_dataloader)

        for epoch in range(self.num_epochs):
            train_loss, train_acc = self.perform_train_pass()
            avg_train_loss = train_loss / trainSteps
            avg_train_acc = train_acc / num_train
            train_loss_list.append(avg_train_loss)
            train_acc_list.append(avg_train_acc)


            val_loss, val_acc = self.perform_val_pass()
            avg_val_loss = val_loss / valSteps
            avg_val_acc = val_acc / num_val
            val_loss_list.append(avg_val_loss)
            val_acc_list.append(avg_val_acc)

            print("[INFO] EPOCH: {}/{}".format(epoch + 1, self.num_epochs))
            print("Train loss: {:.6f}, Train accuracy: {:.4f}".format(
                avg_train_loss, avg_train_acc))
            print("Val loss: {:.6f}, Val accuracy: {:.4f}\n".format(
                avg_val_loss, avg_val_acc))

        self.plot_curve(train_loss_list, train_acc_list, val_loss_list, val_acc_list)

    # Method to perform pass on train set for one epoch
    def perform_train_pass(self):
        train_loss = 0.0
        train_acc = 0.0

        for x, y in self.train_dataloader:
            if torch.cuda.is_available() and self.device == "cuda":
                x = x.to('cuda')
                y = y.to('cuda')

            self.optimizer.zero_grad()
            z = self.net(x)
            loss = self.criterion(z, y)
            loss.backward()
            self.optimizer.step()
            train_loss += loss.item()
            _, yhat = torch.max(z.data, 1)
            train_acc += (yhat == y).sum().item()

        return train_loss, train_acc

    # Method to perform pass on val set for one epoch
    def perform_val_pass(self):
        val_loss = 0.0
        val_acc = 0.0

        with torch.no_grad():
            self.net.eval()

            for x, y in self.val_dataloader:
                if torch.cuda.is_available() and self.device == "cuda":
                    x = x.to('cuda')
                    y = y.to('cuda')

                self.optimizer.zero_grad()
                z = self.net(x)
                loss = self.criterion(z, y)
                val_loss += loss.item()
                _, yhat = torch.max(z.data, 1)
                val_acc += (yhat == y).sum().item()

        return val_loss, val_acc

    # Method to plot loss and accuracy curve.
    def plot_curve(self, train_loss, train_acc, val_loss, val_acc):

        # Make result directory
        result_dir = os.path.join(os.getcwd(), "results", "ViT")
        os.makedirs(result_dir, exist_ok=True)

        fig = plt.figure(figsize=(8, 8))
        plt.subplot(2, 1, 1)
        plt.plot(train_acc, label='Training Accuracy')
        plt.plot(val_acc, label='Validation Accuracy')
        plt.legend(loc='lower right')
        plt.ylabel('Accuracy')
        plt.title('Training and Validation Accuracy')

        plt.subplot(2, 1, 2)
        plt.plot(train_loss, label='Training Loss')
        plt.plot(val_loss, label='Validation Loss')
        plt.legend(loc='upper right')
        plt.ylabel('Cross Entropy')
        plt.title('Training and Validation Loss')
        plt.xlabel('epoch')

        plt.savefig(os.path.join(result_dir, "curve.png"), dpi=fig.dpi)

    # Evaluate model on test dataset
    def evaluate(self):
        y_pred = []
        y_true = []
        with torch.no_grad():
            self.net.eval()

            for x, y in self.test_dataloader:
                if torch.cuda.is_available() and self.device == "cuda":
                    x = x.to('cuda')
                    y = y.to('cuda')

                    y_true.extend(y.data.cpu().numpy())

                z = self.net(x)
                _, preds = torch.max(z, 1)
                y_pred.extend(preds.data.cpu().numpy())

        # Make result directory
        result_dir = os.path.join(os.getcwd(), "results", "ViT")
        os.makedirs(result_dir, exist_ok=True)
        classes = ["adidas", "converse", "nike"]

        cm = confusion_matrix(y_pred, y_true)
        fig = plt.figure(figsize=(12, 8))
        sns.heatmap(cm, annot=True, vmin=0, fmt='g', cmap='Blues', cbar=False)
        plt.xticks(np.arange(3) + .5, classes, rotation=90)
        plt.yticks(np.arange(3) + .5, classes, rotation=0)
        plt.xlabel("Predicted")
        plt.ylabel("Actual")
        plt.title("Confusion Matrix")
        plt.savefig(os.path.join(result_dir, "confusion_matrix.png"), dpi=fig.dpi)