import argparse

from torch.utils.data import DataLoader, random_split
from torchvision.datasets import ImageFolder
from torchvision.transforms import Compose, RandomHorizontalFlip, RandomVerticalFlip, Resize, ToTensor

from models.MyCNN import MyCNNModel
from models.ResNet import ResNetModel
from models.ViT import ViTModel

size = 224

transforms = Compose([
    RandomVerticalFlip(),
    RandomHorizontalFlip(),
    Resize(size),
    ToTensor()
])


def main(args):
    # Load the dataset
    original_ds = ImageFolder(root=args.train_root, transform=transforms)
    test_set = ImageFolder(root=args.test_root, transform=transforms)

    # Split dataset into train and validation set.
    data_len = len(original_ds)
    train_size = int(data_len * 0.90)
    val_size = int(data_len - train_size)
    train_set, val_set = random_split(original_ds, [train_size, val_size])

    train_dataloader = DataLoader(train_set, batch_size=args.batch_size, num_workers=4, shuffle=True)
    val_dataloader = DataLoader(val_set, batch_size=args.batch_size, num_workers=4, shuffle=True)
    test_dataloader = DataLoader(test_set, batch_size=args.batch_size, num_workers=4, shuffle=True)

    # Uncomment one model at a time to train and test

    # MyCNN is custom cnn model of 3 layers
    # Result: This model suffers from overfitting, can be solved by introducing regularize or hyperparameter tuning or
    #         tweaking model architecture.

    # myCnn = MyCNNModel(train_dataloader, val_dataloader, test_dataloader, args.num_class, args.epoch,
    #                    args.learning_rate)
    # myCnn.fit()
    # myCnn.evaluate()

    # ResNet is ResNet 18 model with pretrained weights
    # Result: This model show pretty good performance, accuracy can might be further improved by hyperparameter tuning

    resnet = ResNetModel(train_dataloader, val_dataloader, test_dataloader, args.num_class, args.epoch,
                       args.learning_rate)
    resnet.fit()
    resnet.evaluate()

    # ViT is vision transformer model with pretrained weights
    # Result: This model show pretty poor performance, although for training a bit longer we might see some better
    #         performance. Need to experiment with it.

    # vit = ViTModel(train_dataloader, val_dataloader, test_dataloader, args.num_class, args.epoch,
    #                    args.learning_rate)
    # vit.fit()
    # vit.evaluate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Options")
    parser.add_argument("-train_root", "--train_root", default="data/train", type=str)
    parser.add_argument("-test_root", "--test_root", default="data/test", type=str)
    parser.add_argument("-epoch", "-epoch", default=30, type=int)
    parser.add_argument("-bs", "--batch_size", default=4, type=int)
    parser.add_argument("-lr", "--learning_rate", default=0.0001, type=float)
    parser.add_argument("-num_class", "--num_class", default=3, type=int)
    main(parser.parse_args())
