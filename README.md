## Intro

In this assignment I design a neural network to perform multi class classification for shoes images. I have three 
input classes: nike, adidas, and converse.

<b> Outputs: </b> Please see output in results/ directory.

## Approach

I choose three different neural network architecture and evaluate between them.

1) MyCNN: Custom CNN architecture with 3 hidden layers
2) ResNet: Pretrained ResNet18 with modified final linear.
3) ViT: Pretrained vision transformer with modified head.

#### 1) MyCNN

This architecture has 3 hidden layers \
<b>Output:</b> See output curve in results/MyCNN directory \
<b>Result:</b> This model suffers from overfitting, which can be solved by introducing regularize or hyperparameter 
tuning or tweaking model architecture.

#### 2) ResNet

ResNet 18 model with pretrained weights \
<b>Output:</b> See output curve in results/ResNet directory \
<b>Result:</b> This model show pretty good performance, accuracy can might be further improved by hyperparameter tuning.

#### 3) ViT

Vision transformer model with pretrained weights \
<b>Output:</b> See output curve in results/ViT directory \
<b>Result:</b> This model underfits the data to one class only, although for training a bit longer we might see some 
performance improvement. Need to experiment with it.


## Verdict

<b>Solution:</b> My best solution is ResNet model with transfer learning.


## Meta Information
<b> Time Taken:</b> It took approx 3 hr 17 min to complete the assignment and evaluate models.

